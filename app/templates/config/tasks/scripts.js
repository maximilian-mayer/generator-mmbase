'use strict';

var gulp 			= require('gulp'),
	notify			= require('gulp-notify'),
	browserify 		= require('browserify'),
	buffer			= require('vinyl-buffer'),
	source			= require('vinyl-source-stream'),
	rename			= require('gulp-rename'),
	uglify			= require('gulp-uglify'),
	settings		= require('./../settings');

gulp.task('scripts', function() {
	gulp.src( settings.js.src )
		// .pipe(jshint())
		// .pipe(jshint.reporter('jshint-stylish'));
	return browserify( settings.js.browserify )
		.bundle()
		.pipe(source('main.js'))
		.pipe(buffer())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(uglify())
		.pipe(gulp.dest( settings.js.dist ))
		.pipe(notify({ message: 'Yo, Script task complete.' }));
});
