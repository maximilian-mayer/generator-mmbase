'use strict';

var gulp			= require('gulp'),
	notify			= require('gulp-notify'),
	sass 			= require('gulp-sass'),
	autoprefixer 	= require('gulp-autoprefixer'),
	sourcemaps 		= require('gulp-sourcemaps'),
	rename			= require('gulp-rename'),
	cssimport 		= require("gulp-cssimport"),
	settings 		= require('./../settings');

gulp.task('sass', function() {
	return gulp.src( settings.sass.src )
		.pipe(sourcemaps.init())
		.pipe(rename({ suffix: '.min'}))
		.pipe(sass( settings.sass.output )
			.on('error', sass.logError)
			.on('error', notify.onError('Sass Compile Error!')))
		.pipe(autoprefixer( settings.sass.autoprefixer ))
		.pipe(cssimport( settings.sass.cssimport ))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest( settings.sass.dist ))
		.pipe(notify({ message: 'Yo, Sass task complete.' }));
});