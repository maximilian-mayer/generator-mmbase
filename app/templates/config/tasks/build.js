'use strict';

var gulp		 = require('gulp'),
	settings	 = require('./../settings');

gulp.task(
	'build', [
		'sass',
		'scripts',
		'twig'
	]
);