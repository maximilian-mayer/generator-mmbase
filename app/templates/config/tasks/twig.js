'use strict';

var gulp		= require('gulp'),
	notify		= require('gulp-notify'),
	twig 		= require('gulp-twig'),
	settings	= require('./../settings');

gulp.task('twig', function () {
	return gulp.src( settings.twig.src )
		.pipe(twig())
		.pipe(gulp.dest( settings.twig.dist ))
		.pipe(notify({ message: 'Yo, Twig task complete.' }));
});