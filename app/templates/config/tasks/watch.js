'use strict';

var gulp 		= require('gulp'),
	livereload 	= require('gulp-livereload'),
	settings 	= require('./../settings');

gulp.task('watch', function() {
	livereload.listen();

	gulp.watch(settings.sass.src, ['sass']).on('change', livereload.changed);
	gulp.watch(settings.js.src, ['scripts']).on('change', livereload.changed);
	gulp.watch(settings.twig.src, ['twig']).on('change', livereload.changed);
});