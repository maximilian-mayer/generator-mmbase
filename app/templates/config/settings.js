'use strict';

<% if (projectUsage == 'Prototyping' ) { %>
	var src = './_src';
	var dist = './_dist';
	var assets = dist + '/assets';
<% } if (projectUsage == 'TYPO3' ) { %>
	var src = './_src';
	var dist = './_dist';
	var assets = dist + '/assets';
<% } %>

module.exports = {
	sass: {
		src: src + '/sass/**/*.scss',
		dist: assets + '/css/',
		autoprefixer: {
			browsers: ['last 2 versions', '> 5%', 'ie 9']
		},
		output: {
			outputStyle: 'compressed',
			precision: 10
		},
		cssimport: ''
	},

	js: {
		src: src + '/js/**/*.js',
		dist: assets + '/js/',
		browserify: {
			entries: src + '/js/main.js',
			debug: true
		}
	},

	twig: {
		src: src + '/*.twig',
		dist: dist + '/'
	}
}