'use strict';

var $ = require('jquery'),
	responsiveNavigation = require('./responsiveNavigation');

// // make jQuery available for plugins
// global.jQuery = $;
// global.$ = $;
// require('viewportChecker');

$(document).ready(function() {

	new responsiveNavigation();
	// new pageTransition();

});
