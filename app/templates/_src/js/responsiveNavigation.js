module.exports = function() {
	'use strict';

	var $ = require('jquery');
	require('../../node_modules/jquery.mmenu/dist/js/jquery.mmenu.all.min.umd');

	function appendMenu(index, item) {
		responsiveNavigation += $(item).html();
	}

	var mainNavigation = $('.default-navigation > ul'),
		metaNavigation = $('.default-meta > ul'),
		responsiveNavigation = '<ul>',
		menu = $('#menu');

	mainNavigation.each(appendMenu);
	metaNavigation.each(appendMenu);
	responsiveNavigation += '</ul>';

	$('html').addClass('responsive-menu');
	menu.html(responsiveNavigation);
	menu.mmenu({
		'offCanvas': {
			'position': 'right'
		}
	});
};
