'use strict';
var util   = require('util');
var path   = require('path');
var yeoman = require('yeoman-generator');
var yosay  = require('yosay');
var chalk  = require('chalk');
var mkdirp = require('mkdirp');

var mmBaseGenerator = yeoman.generators.Base.extend({

	init: function () {
		this.pkg = require('../package.json');
	},

	askFor: function () {
		var done = this.async();

		// Have Yeoman greet the user.
		this.log(yosay(
			'Welcome to ' + chalk.red('mmBase') + ' generator!'
		));

		var prompts = [
			{
				type:    'input',
				name:    'projectName',
				message: 'Please give your project a name (without Spaces)',
				default: 'mmBase'
			}, {
				type:    'input',
				name:    'projectDescription',
				message: 'Short description of the Project',
				default: 'undefined'
			}, {
				type:    'list',
				name:    'projectUsage',
				message: 'Which purpose does this Project have? Choose the appropriate option',
				choices: [
					"Prototyping",
					"TYPO3"
				]
			}, {
				type:    'input',
				name:    'projectVersion',
				message: 'Project Version Number',
				default: '0.0.1'
			}, {
				type:    'input',
				name:    'projectAuthor',
				message: 'Project Author or company',
				default: 'undefined'
			}, {
				type:    'input',
				name:    'projectRepo',
				message: 'Git Repo URL',
				default: 'http://...'
			}
		];

		this.prompt(prompts, function (props) {
			this.projectName          = props.projectName;
			this.projectDescription   = props.projectDescription;
			this.projectUsage         = props.projectUsage;
			this.projectVersion       = props.projectVersion;
			this.projectAuthor        = props.projectAuthor;
			this.projectRepo          = props.projectRepo;
			done();
		}.bind(this));

	},

	app: function () {
		// move src folder
		this.directory('_src/',        './_src/');
		this.directory('config/',      './config/');

		// if ( this.projectUsage === 'Prototyping' ) {
		// 	this.directory('___src/templates/prototyping/', '___src/templates/');
		// }

		// if ( this.projectUsage === 'WordPress' ) {
		// 	this.directory('___src/templates/wordpress/', '___src/templates/');
		// }

		// if ( this.projectUsage === 'CraftCMS' ) {
		// 	this.directory('___src/templates/craftcms/', '___src/templates/');
		// }
	},

	projectfiles: function () {
		this.copy('_package.json',          'package.json');
		this.copy('_gulpfile.js',           'gulpfile.js');
		this.copy('_readme.md',             'readme.md');
		this.copy('_gitignore',             '.gitignore');
		this.copy('editorconfig',           '.editorconfig');
	},

	install: function () {
		console.log('Install NPM Modules.');
		console.log('Give me a moment to do that…');
		this.installDependencies({
			bower: false,
			npm: true
		});
	}

});

module.exports = mmBaseGenerator;
